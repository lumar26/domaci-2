package auto_radnja;

import auto_radnja.gume.AutoGuma;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public abstract class RadnjaTest {

    protected Radnja r;

    @Test
    void dodajGumu() {
        AutoGuma g = new AutoGuma("Tigar:  All seasons", 16, 200, 60);
        r.dodajGumu(g);
        assertTrue(r.vratiSveGume().contains(g));
    }

    @Test
    void dodajGumuNull() {
        assertThrows(NullPointerException.class, () -> r.dodajGumu(null));
    }

    @Test
    void dodajGumuAlreadyExists() {
        AutoGuma g = new AutoGuma();
        g.setMarkaModel("Tigar");
        r.dodajGumu(g);
        assertThrows(RuntimeException.class, () -> r.dodajGumu(g));
    }

    @Test
    void pronadjiGumuSingle(){
        AutoGuma g1 = new AutoGuma("Tigar", 16, 200, 60);
        AutoGuma g3 = new AutoGuma("Yokohoma", 19, 170, 55);
        AutoGuma g4 = new AutoGuma("Michelin", 19, 170, 55);
        r.dodajGumu(g1);
        r.dodajGumu(g3);
        r.dodajGumu(g4);
        assertEquals(1, r.pronadjiGumu("Tigar").size());
    }

    @Test
    void pronadjiGumu(){
        AutoGuma g1 = new AutoGuma("Tigar", 16, 200, 60);
        AutoGuma g2 = new AutoGuma("Tigar", 19, 170, 55);
        AutoGuma g3 = new AutoGuma("Yokohoma", 19, 170, 55);
        AutoGuma g4 = new AutoGuma("Michelin", 19, 170, 55);
        List<AutoGuma> result = new ArrayList<>();
        result.add(g2);
        result.add(g1);
        r.dodajGumu(g1);
        r.dodajGumu(g2);
        r.dodajGumu(g3);
        r.dodajGumu(g4);
        assertEquals(result, r.pronadjiGumu("Tigar"));
    }

    @Test
    void pronadjiGumuMarkaModelNull() {
        assertNull(r.pronadjiGumu(null));
    }

    @Test
    void pronadjiGumuEmpty() {
        assertEquals(0, r.pronadjiGumu("Tigar").size());
    }

    @Test
    void pronadjiGumuNotExist() {
        AutoGuma g1 = new AutoGuma("Tigar", 16, 200, 60);
        AutoGuma g2 = new AutoGuma("LingLong", 19, 170, 55);
        AutoGuma g3 = new AutoGuma("Yokohoma", 19, 170, 55);
        AutoGuma g4 = new AutoGuma("Michelin", 19, 170, 55);
        r.dodajGumu(g1);
        r.dodajGumu(g2);
        r.dodajGumu(g3);
        r.dodajGumu(g4);
        assertEquals(0, r.pronadjiGumu("Dummy").size());
    }

    @Test
    void vratiSveGume() {
        AutoGuma g1 = new AutoGuma("Tigar", 16, 200, 60);
        AutoGuma g2 = new AutoGuma("LingLong", 19, 170, 55);
        AutoGuma g3 = new AutoGuma("Yokohoma", 19, 170, 55);
        AutoGuma g4 = new AutoGuma("Michelin", 19, 170, 55);
        r.dodajGumu(g1);
        r.dodajGumu(g2);
        r.dodajGumu(g3);
        r.dodajGumu(g4);

        assertEquals(4, r.vratiSveGume().size());
        assertTrue(r.pronadjiGumu(g1.getMarkaModel()).contains(g1));
        assertTrue(r.pronadjiGumu(g2.getMarkaModel()).contains(g2));
        assertTrue(r.pronadjiGumu(g3.getMarkaModel()).contains(g3));
        assertTrue(r.pronadjiGumu(g4.getMarkaModel()).contains(g4));

    }
}