package auto_radnja;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

class VulkanizerskaRadnjaTest extends RadnjaTest {

    @BeforeEach
    void setUp() {
        r = new VulkanizerskaRadnja();
    }

    @AfterEach
    void tearDown() {
        r = null;
    }
}