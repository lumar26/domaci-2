package auto_radnja.gume;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class AutoGumaTest {

    private AutoGuma guma;

    @BeforeEach
    void setUp() {
        guma = new AutoGuma();
    }

    @AfterEach
    void tearDown() {
        guma = null;
    }

    @Test
    void setMarkaModel() {
        guma.setMarkaModel("Tigar: All season");
        assertEquals("Tigar: All season", guma.getMarkaModel());
    }

    @Test
    void setMarkaModelNull() {
        assertThrows(NullPointerException.class, () -> guma.setMarkaModel(null));
    }

    @ParameterizedTest
    @CsvSource({
            "t",
            "tr"
    })
    void setMarkaModelIllegalArgument(String markaModel) {
        assertThrows(IllegalArgumentException.class, () -> guma.setMarkaModel(markaModel));

    }

    @Test
    void setPrecnik() {
        guma.setPrecnik(15);
        assertEquals(15, guma.getPrecnik());
    }


    @ParameterizedTest
    @CsvSource({
            "11",
            "12",
            "23",
            "24"
    })
    void setPrecnikIllegalArgument(String precnik) {
        int p = Integer.parseInt(precnik);
        assertThrows(IllegalArgumentException.class, () -> guma.setPrecnik(p));
    }

    @Test
    void setSirina() {
        guma.setSirina(200);
        assertEquals(200, guma.getSirina());
    }

    @Test
    void setSirinaIllegalArgumentII() {
        for (int i = 0; i < 500; i++) {
            if (i < 135 || i > 355) {
                int temp = i;
                assertThrows(IllegalArgumentException.class, () -> guma.setSirina(temp));
            }
        }
    }

    @ParameterizedTest
    @CsvSource({
            "134",
            "133",
            "356",
            "357"
    })
    void setSirinaIllegalArgument(int p) {
        assertThrows(IllegalArgumentException.class, () -> guma.setSirina(p));
    }

    @Test
    void setVisina() {
        guma.setVisina(50);
        assertEquals(50, guma.getVisina());
    }

    @Test
    void setVisinaII() {
        guma.setVisina(50);
        assertEquals(50, guma.getVisina());
    }

    @Test
    void setVisinaIllegalArgument() {
        for (int i = 0; i < 500; i++) {
            if (i < 25 || i > 95) {
                int temp = i;
                assertThrows(IllegalArgumentException.class, () -> guma.setVisina(temp));
            }
        }
    }

    @Test
    void testToString() {
        guma.setVisina(50);
        guma.setMarkaModel("Tigar: All seasons");
        guma.setSirina(200);
        guma.setPrecnik(16);

        String desc = guma.toString();
        assertTrue(desc.contains(Integer.toString(guma.getVisina())));
        assertTrue(desc.contains(Integer.toString(guma.getSirina())));
        assertTrue(desc.contains(Integer.toString(guma.getPrecnik())));
        assertTrue(desc.contains(guma.getMarkaModel()));

    }

    @ParameterizedTest
    @CsvSource({
            "50, Tigar: All seasons, 200, 16, 50, Tigar: All seasons, 200, 16, true",
            "50, Tigar: All seasons, 200, 16, 60, Tigar: All seasons, 200, 16, false",
            "50, Tigar: All seasons, 200, 16, 50, Tiga: All seasons, 200, 16, false",
            "50, Tigar: All seasons, 200, 16, 50, Tigar: All seasons, 199, 16, false",
            "50, Tigar: All seasons, 200, 16, 50, Tigar: All seasons, 200, 15, false",
    })
    void testEquals(int visina1, String markamodel1, int sirina1, int precnik1,
                    int visina2, String markamodel2, int sirina2, int precnik2,
                    boolean result) {
        guma.setVisina(visina1);
        guma.setMarkaModel(markamodel1);
        guma.setSirina(sirina1);
        guma.setPrecnik(precnik1);

        AutoGuma guma2 = new AutoGuma(markamodel2, precnik2, sirina2, visina2);

        assertEquals(result, guma.equals(guma2));
    }

    @Test
    void testAutoGumaStringIntIntInt(){
        guma = new AutoGuma("Tigar:  All seasons", 16, 200, 60);
        assertEquals("Tigar:  All seasons", guma.getMarkaModel());
        assertEquals(16, guma.getPrecnik());
        assertEquals(60, guma.getVisina());
        assertEquals(200, guma.getSirina());
    }

}