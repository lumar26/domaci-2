package auto_radnja;

import auto_radnja.gume.AutoGuma;

import java.util.List;

/**
 * Predstavlja radnju u kojoj postoje auto gume i omogućava osnovne operacije nad gumama: dodavanje i pronalaženje
 *
 * @author lumar26
 * @version 1.0
 */
public interface Radnja {
    /**
     * Dodaje novu auto gumu u kolekciju guma koje se nalaze u radnji
     *
     * @param a Nova guma koju treba dodati
     * @throws NullPointerException Ukoliko je prosleđena guma null
     * @throws RuntimeException Ukoliko prosleđena guma već postoji u kolekciji
     */
    void dodajGumu(AutoGuma a);

    /**
     * Pronalazi sve gume iste marke i modela u radnji
     *
     * @param markaModel Uslov pretrage po kom se pronalazi guma u radnji
     * @return Vraća listu guma koje su pronađene na osnovu zadatog uslova
     */
    List<AutoGuma> pronadjiGumu(String markaModel);

    /**
     * Vraća sve gume koje u radnji trenutno postoje
     *
     * @return KOlekcija svih guma u radnji
     */
    List<AutoGuma> vratiSveGume();
}
