package auto_radnja.gume;

/**
 * Predstavlja jednu automobilsku gumu
 *
 * @author Luka Marinković 2018/0074
 * @version 1.0
 */
public class AutoGuma {
    /**
     * Naziv marke i modela gume
     */
    private String markaModel = null;
    /**
     * Celobrojna vrednost prečnika gume
     */
    private int precnik = -1;
    /**
     * Celobrojna vrednost širine gume
     */
    private int sirina = -1;

    /**
     * Celobrojna vrednost visine gume
     */
    private int visina = -1;

    /**
     * Postavlja vrednosti atributa na default vrednosti
     */
    public AutoGuma() {
    }

    /**
     * Postavlja vrednosti atributa na prosleđene vrednosti
     *
     * @param markaModel Marka i model
     * @param precnik Prečnik gume
     * @param sirina Širina gume
     * @param visina Visina gume
     */
    public AutoGuma(String markaModel, int precnik, int sirina, int visina) {
        super();
        setMarkaModel(markaModel);
        setPrecnik(precnik);
        setSirina(sirina);
        setVisina(visina);
    }

    /**
     * @return Trenutna vrednost marke i modela
     */
    public String getMarkaModel() {
        return markaModel;
    }

    /**
     * Postavlja novu vrednost naziva marke i modela
     *
     * @param markaModel Nova vrednost za naziv marke i modela
     * @throws NullPointerException U slučaju da je prosleđena vrednost naziva marke i modela jednaka null
     * @throws IllegalArgumentException U slučaju da prosleđena vrednost naziva marke i modela ima manje od 3 karaktera
     */
    public void setMarkaModel(String markaModel) {
        if (markaModel == null)
            throw new NullPointerException("Morate uneti marku i model");
        if (markaModel.length() < 3)
            throw new IllegalArgumentException
                    ("Marka i model moraju sadrzati bar 3 znaka");
        this.markaModel = markaModel;
    }

    /**
     * @return Trenutna vrednost prečnika gume
     */
    public int getPrecnik() {
        return precnik;
    }

    /**
     * Postavlja novu vrednost prečnika gume
     *
     * @param precnik Nova vrednost za prečnik gume
     * @throws IllegalArgumentException Ukoliko prečnik nije u opsegu [13,22]
     */
    public void setPrecnik(int precnik) {
        if (precnik < 13 || precnik > 22)
            throw new IllegalArgumentException("Precnik van opsega");
        this.precnik = precnik;
    }
    /**
     * @return Trenutna vrednost širine gume
     */
    public int getSirina() {
        return sirina;
    }

    /**
     * Postavlja novu vrednost širine gume
     *
     * @param sirina Nova vrednost širine gume
     * @throws IllegalArgumentException Ukoliko širina nije u opsegu [135,355]
     */
    public void setSirina(int sirina) {
        if (sirina < 135 || sirina > 355)
            throw new IllegalArgumentException("Sirina van opsega");
        this.sirina = sirina;
    }

    /**
     * @return Trenutna vrednost visine gume
     */
    public int getVisina() {
        return visina;
    }

    /**
     * Postavlja novu vrednost visine gume
     *
     * @param visina Nova vrednost visine gume
     * @throws IllegalArgumentException Ukoliko širina nije u opsegu [135,355]
     */
    public void setVisina(int visina) {
        if (visina < 25 || visina > 95)
            throw new IllegalArgumentException("Visina van opsega");
        this.visina = visina;
    }

    /**
     * @return Opis auto gume koji sadrži: marku i model; prečnik; širinu; visinu
     */
    @Override
    public String toString() {
        return "AutoGuma [markaModel=" + markaModel + ", precnik=" + precnik +
                ", sirina=" + sirina + ", visina=" + visina + "]";
    }

    /**
     * Poredi trenutni objekat auto gume sa prosleđenim objektom
     *
     * @param obj Objekat sa kojim se upoređuje auto guma
     * @return true, ukoliko prosleđeni objekat ima istu marku i model, visinu, prečnik i širinu kao i auto guma sa kojom se upoređuje
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AutoGuma other = (AutoGuma) obj;
        if (markaModel == null) {
            if (other.markaModel != null)
                return false;
        } else if (!markaModel.equals(other.markaModel))
            return false;
        if (precnik != other.precnik)
            return false;
        if (sirina != other.sirina)
            return false;
        if (visina != other.visina)
            return false;
        return true;
    }
}
