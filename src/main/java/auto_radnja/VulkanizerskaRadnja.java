package auto_radnja;

import auto_radnja.gume.AutoGuma;

import java.util.LinkedList;
import java.util.List;

/**
 * Predstavlja radnju koja radi sa auto gumama i sadrži osnovne operacije za rad sa gumama na stanju
 *
 * @author lumar26
 * @version 1.0
 */
public class VulkanizerskaRadnja implements Radnja {
    /**
     * Kolekcija guma koja predstavlja trenutno stanje guma u radnji
     */
    private List<AutoGuma> gume =
            new LinkedList<AutoGuma>();

    @Override
    public void dodajGumu(AutoGuma a) {
        if (a == null)
            throw new NullPointerException("Guma ne sme biti null");
        if (gume.contains(a))
            throw new RuntimeException("Guma vec postoji");
//        gume.addFirst(a); // koristim jdk 11 pa ova metoda nije dostupna
        gume.add(0, a);
    }

    @Override
    public List<AutoGuma> pronadjiGumu(String markaModel) {
        if (markaModel == null)
            return null;
        List<AutoGuma> novaLista = new LinkedList<AutoGuma>();
        for (int i = 0; i < gume.size(); i++)
            if (gume.get(i).getMarkaModel().equals(markaModel))
                novaLista.add(gume.get(i));
        return novaLista;
    }

    @Override
    public List<AutoGuma> vratiSveGume() {
        return gume;
    }
}
